<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
include $_SERVER['HTTP_HOST'] == 'vrpm.dev' 
    ? '.env.local.php' 
    : '.env.php';

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p~i]o[7U;aVQWHT OkkBqzLbM,a3oMNcIWw_7}CI^jUJ|>81]@otGy0L,zYi0<=d');
define('SECURE_AUTH_KEY',  'qp62F&Qvw+g,MqR_RL~QV7qbk|<V{AD8J&M}kus]khSp[d>Ojb9T>jVT/B*zOd70');
define('LOGGED_IN_KEY',    'J4N+6_L,l01ngF3U_B 2@mSnHQ;1(f1|^]@R#=f&}*va:hj`Qw-DYJew@c%E{?[h');
define('NONCE_KEY',        '#ott;e8 jvRkL}^_VSe$7d`Qu!E>6:^9r@<p-~.vlX}Y!c#9!G!JH:gH)?5q!MTZ');
define('AUTH_SALT',        'T&9f}WCj{,8>@r`u)V*vV(*8-~0cTP0Ov@7B3=~P7NRLR&zTL?Z^_oT3/E;trb3$');
define('SECURE_AUTH_SALT', '0@~X,e4nx;Bbzzzhg}-#.5(-De>Mw`=C<p87m=d#G&7gVHJ_:TT }r?]]wO&~:FB');
define('LOGGED_IN_SALT',   'bK@k[9TDJ/b{3JSN,]xW9fl-Q}E#2+[#fk6m;lQIu,)K^ME1(=d!vR)!AH>L76m6');
define('NONCE_SALT',       '>88SDA%hI-WzygN#TY7Um[#Y!o3vVd)eTGyiVy $>OMrcr1[K3a]@4@4 ~[Q-(MV');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
